
# Changelog for Messages Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.4.0] - 2022-09-22

- Ported to GWT 2.10.0 and Java 11
- Ported to Git

## [v2.3.0] - 2017-11-12

- Ported to GWT 2.8.1

## [v2.2.0] - 2016-11-25

 - removed ASL Session

## [v2.1.0] - 2016-10-03
 
 - Added feature to display VRE Groups and VRE Managers

## [v2.0.0] - 2016-06-30
 
 - Added feature to display tagged VRE Groups

## [v1.0.0] - 2014-09-09

- First release
